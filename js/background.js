console.log('background script started')

const tabs = []

const onMessageListener = (message, sender, resolve) => {
  getTabs()
  getAllCommands()

  const found = tabs.find(tab => tab === sender.tab.id)
  console.log('browser.runtime.onMessage', message, sender, found)

  if (!found) {
    tabs.push(sender.tab.id)
  }

  resolve({counter: tabs.length})

  return true
}

const getTabs = (query = {}) => {
  browser.tabs.query(query).then((tabs) => {
    console.log(tabs)
  })
}

const getAllCommands = () => {
  browser.commands.getAll().then((commands) => {
    console.log(commands)
  })
}

const onCommandListener = (command) => {
  console.log('onCommandListener', command)
}

browser.runtime.onMessage.addListener(onMessageListener)

browser.commands.onCommand.addListener(onCommandListener);