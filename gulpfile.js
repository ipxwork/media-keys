const gulp = require('gulp')
const webExt = require('web-ext').default;

gulp.task('default', () => {
  return console.info('run default task')
})

gulp.task('web-ext', () => {
  return webExt.cmd.run({
    sourceDir: __dirname,
    browserConsole: true,
    startUrl: 'www.mozilla.com',
    verbose: true
  }, {shouldExitProgram: true})
})